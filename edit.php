﻿<form action="main.php">
<input type="submit" value="BACK" class="standart_button">
</form>

<meta charset="utf-8">

<?php
require_once 'config.php';
echo "<link rel='stylesheet' href='style.css'>";

if( isset($_POST['bank_name']) && isset($_POST['new_bank_name']) && isset($_POST['interest_rate']) && isset($_POST['max_loan']) && isset($_POST['min_down_payment']) && isset($_POST['loan_term_month'])) {

	$bank_name = htmlentities(mysqli_real_escape_string($dbcon, $_POST['bank_name']));

	$new_bank_name = htmlentities(mysqli_real_escape_string($dbcon, $_POST['new_bank_name']));
	$interest_rate = htmlentities(mysqli_real_escape_string($dbcon, $_POST['interest_rate']));
	$max_loan = htmlentities(mysqli_real_escape_string($dbcon, $_POST['max_loan']));
	$min_down_payment = htmlentities(mysqli_real_escape_string($dbcon, $_POST['min_down_payment']));
	$loan_term_month = htmlentities(mysqli_real_escape_string($dbcon, $_POST['loan_term_month']));
	


	$query1 = "select * from bank_info WHERE bank_name = '$bank_name'";
	$result1 = mysqli_query($dbcon, $query1) or die("ERROR " . mysqli_error($dbcon));
	$ver=mysqli_query($dbcon,$query1);

	while(list( $bank_name) = mysqli_fetch_row($ver)) {
		if("$bank_name"){
			$query = "UPDATE bank_info 
			SET bank_name = '$new_bank_name', interest_rate = '$interest_rate', max_loan = '$max_loan', min_down_payment = '$min_down_payment', loan_term_month = '$loan_term_month' WHERE bank_name = '$bank_name'";

			$result = mysqli_query($dbcon, $query) or die("Error " . mysqli_error($dbcon));
			if ($result) {
    			echo "<h4 class='title' style='color:red;'>Data is succesfully changed!</h4>";
			}
		}
	}

	mysqli_close($dbcon); 
} 
?>

<form method = "POST">
	<h2 class='title'>EDIT BANK INFO</h2><hr>
<pre class='title'>Enter the FULL NAME of the bank that will be changed:</pre>
<input type="text" name="bank_name"  autofocus required>
<br><br>


<table class='table_col'>
		<colgroup>
      		<col style='background:#C7DAF0;'>
  		</colgroup>
		
		<tr>
    		<td>NEW BANK NAME:</td>
			<td> <input type="text" name="new_bank_name" required> </td>
    	</tr> 
    	<tr>
			<td>INTEREST RATE:</td>
			<td> <input type="" name="interest_rate" step="0.01"required></td>
		</tr>
		<tr>
			<td>MAXIMUM LOAN:</td>
			<td><input type="number" name="max_loan" required></td>
		</tr>
		<tr>
			<td>MINIMUM DOWN PAYMENT:</td>
			<td><input type="number" name="min_down_payment" required></td>
		</tr>
		<tr>
			<td>LOAN MONTH (amount of month):</td>
			<td><input type="number" name="loan_term_month" min="1" max="100" required></td>
		</tr>
</table>
<br>


<input type="submit" value="SAVE" class="standart_button"><br>
</form>
