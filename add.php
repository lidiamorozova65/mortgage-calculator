﻿<form action="main.php">
<input type="submit" value="BACK" class="standart_button">
</form>

<meta charset="utf-8">

<?php
require_once 'config.php';
echo "<link rel='stylesheet' href='style.css'>";

if( isset($_POST['bank_name']) && isset($_POST['interest_rate']) && isset($_POST['max_loan']) && isset($_POST['min_down_payment']) && isset($_POST['loan_term_month'])) {
	$bank_name = htmlentities(mysqli_real_escape_string($dbcon, $_POST['bank_name']));
	$interest_rate = htmlentities(mysqli_real_escape_string($dbcon, $_POST['interest_rate']));
	$max_loan = htmlentities(mysqli_real_escape_string($dbcon, $_POST['max_loan']));
	$min_down_payment = htmlentities(mysqli_real_escape_string($dbcon, $_POST['min_down_payment']));
	$loan_term_month = htmlentities(mysqli_real_escape_string($dbcon, $_POST['loan_term_month']));

	$query = "INSERT INTO bank_info VALUES ('$bank_name', '$interest_rate', '$max_loan', '$min_down_payment', '$loan_term_month')";
	$result = mysqli_query($dbcon, $query) or die("<h4 class='title'> Bank with this name is already exsists. Please try again!</h4> <form action='add.php'>
<input type='submit' value='TRY AGAIN' class='standart_button'>
</form>");
	
	if ($result) {
		echo "<h4 class='title' style='color:red;'>New bank is succesfully added!</h4>";
	}
	mysqli_close($dbcon);
} 
?>

<form method = "POST">
<h2 class='title'>ADD NEW BANK</h2><hr>
<pre class='title'>Enter new bank info:</pre>

<table class='table_col'>
		<colgroup>
      		<col style='background:#C7DAF0;'>
  		</colgroup>
		
		<tr>
    		<td>BANK NAME:</td>
			<td> <input type="text" name="bank_name"  type="text" autofocus required> </td>
    	</tr> 
    	<tr>
			<td>INTEREST RATE:</td>
			<td> <input type="number" name="interest_rate" step="0.01" required></td>
		</tr>
		<tr>
			<td>MAXIMUM LOAN:</td>
			<td><input type="number" name="max_loan" required></td>
		</tr>
		<tr>
			<td>MINIMUM DOWN PAYMENT:</td>
			<td><input type="number" name="min_down_payment" required></td>
		</tr>
		<tr>
			<td>LOAN MONTH (amount of month):</td>
			<td><input type="number" name="loan_term_month" min="1" max="100" required></td>
		</tr>
</table>
<br>
<input type="submit" value="SAVE" class="standart_button"><br>
</form>