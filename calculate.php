﻿<form action="main.php">
	<input type="submit" value="BACK" class="standart_button">
</form>

<meta charset="utf-8">

<?php
include "config.php";
echo "<link rel='stylesheet' href='style.css'>";


$query = "select * from bank_info";
$ver=mysqli_query($dbcon,$query);


if (!$ver) {
	echo "<P>Connection is lost</P>"; 
	exit(mysqli_error());
}

echo "<form method = 'POST'>
	<h2 class='title'>MORTGAGE CALCULATOR</h2>
	<hr>

	<table class='table_col'>
		<colgroup>
		<col style='background:#C7DAF0;'>
		</colgroup>

		<tr>
		<td><pre class='title'>BANK NAME:</pre></td>
		<td><select name='drop_down'>"; 


while(list( $bank_name) = mysqli_fetch_row($ver)) {
	echo "<option value='$bank_name'> $bank_name</option>";
};

echo "</select><td>
		<tr>
		<td>INITIAL LOAN:</td>
		<td> <input type='text' name='user_initial_loan' required></td>
		</tr>

		<tr>
		<td> DOWN PAYMENT:</td>
		<td><input type='text' name='user_down_payment' required></td>
		</tr>

		<tr>
		<td> LOAN TERM (amount of time you plan to give borrowed money back):</td>
		<td><input type='text' name='user_loan_term' required></td>
		</tr>
	</table>
	<br>";

echo "<input type='submit' value='count monthly payment' class='standart_button'><br>
</form>";

if(isset($_POST['drop_down']) && isset($_POST['user_initial_loan']) && isset($_POST['user_down_payment']) && isset($_POST['user_loan_term'])){
	$rez = $_POST['drop_down'];
	$user_initial_loan = $_POST['user_initial_loan'];
	$user_down_payment = $_POST['user_down_payment'];
	$user_loan_term = $_POST['user_loan_term'];

	$query_count = "select * from bank_info where bank_name='$rez'";
	$ver_count = mysqli_query($dbcon,$query_count);

	while(list( $bank_name, $interest_rate, $max_loan, $min_down_payment, $loan_term_month) = mysqli_fetch_row($ver_count)) {

		if($user_initial_loan>$max_loan){
			echo "<h4 class='title'>Initial loan should be less than $max_loan </h4>";
		}else if($user_loan_term>$loan_term_month){
			echo "<h4 class='title'>Loan term should be less than $loan_term_month </h4>";
		}else if($user_down_payment<$min_down_payment){
			echo "<h4 class='title'>Down payment should be more than $min_down_payment </h4>";
		}else{
			$r = $interest_rate/100/12;
			$base = round(pow(1+$r,$user_loan_term)-1, 2);
			
			if($base ==0){
				echo "check that you entered right data and try again!";
			}else{
			$month_payment = round($user_initial_loan*$r*pow($r+1,$user_loan_term)/$base, 2);

			echo "<h3 class ='title'> Yours monthly payment is:  $month_payment</h3>";
			}
			
		}

	};

}
?>
