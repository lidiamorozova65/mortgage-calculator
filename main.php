﻿<h1 class="title"> Mortgage calculator </h1>
<hr>

<?php
include "config.php";
echo "<link rel='stylesheet' href='style.css'>";

$query = "select * from bank_info";
$ver=mysqli_query($dbcon,$query);

if (!$ver) {
	echo "<P> Не вдалося встановити зв'язок з таблицею</P>"; 
	exit(mysqli_error());
}

echo "<h2 class='title'><b>ALL BANKS</b></h2>"; 

echo "<table class='table_col'>
		<colgroup>
      		<col style='background:#C7DAF0;'>
  		</colgroup>
		<tr>
    		<th>BANK NAME</th>
    		<th>INTEREST RATE</th>
    		<th>MAX LOAN</th>
    		<th>MIN DOWN PAYMENT</th>
    		<th>LOAN TERM (month)</th>
    	</tr> ";

while(list( $bank_name, $interest_rate, $max_loan, $min_down_payment, $loan_term_month) = mysqli_fetch_row($ver)) {
	echo " <tr>
	<td> $bank_name </td>
	<td> $interest_rate %</td>
	<td> $max_loan </td>
	<td> $min_down_payment </td>
	<td> $loan_term_month </td>
	</tr>";
};
echo "</table>";

?>



<section>
<pre class="title">Choose the option:</pre>
<table class="options"> 
	<tr> 
		<td> 
			<form action="show.php"> 
				<input type="submit" value="show banks" class="standart_button">
			</form>
		</td>
		<td>
			<form action="add.php">
				<input type="submit" value="add new bank" class="standart_button">
			</form>
		</td>
		<td>
			<form action="edit.php">
				<input type="submit" value="edit bank info" class="standart_button">
			</form>
		</td>
		<td>
			<form action="delete.php">
				<input type="submit" value="remove bank" class="standart_button">
			</form>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td>
			<form action="calculate.php">
				<input type="submit" value="calculate monthly payment" class="standart_button">
			</form>
		</td>
	</tr>
</table>
</section>
