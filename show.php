﻿</form>
<form action="main.php">
<input type="submit" value="BACK" class="standart_button">
</form>

<meta charset="utf-8">

<?php
include "config.php";
echo "<link rel='stylesheet' href='style.css'>";

$query = "select * from bank_info";
$ver=mysqli_query($dbcon,$query);

if (!$ver) {
	echo "<P>Connection is lost</P>"; 
	exit(mysqli_error());
}

echo "<h2 class='title'><b>BANKS</b></h2><hr>"; 

echo "<table class='table_col'>
		<colgroup>
      		<col style='background:#C7DAF0;'>
  		</colgroup>
		<tr>
    		<th>BANK NAME</th>
    		<th>INTEREST RATE</th>
    		<th>MAX LOAN</th>
    		<th>MIN DOWN PAYMENT</th>
    		<th>LOAN TERM (month)</th>
    	</tr> ";

while(list( $bank_name, $interest_rate, $max_loan, $min_down_payment, $loan_term_month) = mysqli_fetch_row($ver)) {
	echo " <tr>
	<td> $bank_name </td>
	<td> $interest_rate %</td>
	<td> $max_loan </td>
	<td> $min_down_payment </td>
	<td> $loan_term_month </td>
	</tr>";
};
echo "</table>";

?>